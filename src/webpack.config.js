const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const globImporter = require('node-sass-glob-importer')
const webpack = require('webpack')
const path = require('path')
const fs = require('fs-extra')

function generateHtmlPlugins (templateDir) {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir))
    return templateFiles.map(item => {

        const parts = item.split('.')
        const name = parts[0]
        const extension = parts[1]

        var pathIn = '../pages'
        if ( name == 'index' ) pathIn = '..'

        return new HtmlWebpackPlugin({
            filename: `${pathIn}/${name}.html`,
            template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
        })
    })
}

const htmlPlugins = generateHtmlPlugins('./pug/views')

module.exports = {
  entry: {
    script: './js/app.js',
  },

  output: {
    filename: 'script.js',
    path: path.resolve(__dirname, '../public/components'),
  },

  module: {
    rules: [
      {
        test: /\.(pug|jade)$/,
        loaders: ['html-loader', 'pug-html-loader'],
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.wav$|\.ico$|\.mp3$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            outputPath: '../',
            publicPath: './',
            // useRelativePath: true
          },
        }]
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: '/fonts',
            publicPath: 'fonts',
          }
        }]
      },
      {
        test: /\.s(a|c)ss$/,
        use: ExtractTextPlugin.extract([
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              importer: globImporter()
            }
          }
        ])
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      'window.jQuery': 'jquery',
    }),
    new ExtractTextPlugin('style.css')
  ].concat(htmlPlugins)
}