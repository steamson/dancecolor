import '../sass/app.sass'
import 'slick-carousel'

$(document).ready(function() {

    var paints = $(".plates_carousel").slick({
        slidesToShow: 2,
        slidesToScroll: 1,

        arrows: false,
        infinite: true,
    })

    $('.plates_carousel-nav .round_btns-nav__prev').on('click', function () { paints.slick('slickPrev') })
    $('.plates_carousel-nav .round_btns-nav__next').on('click', function () { paints.slick('slickNext') })

})